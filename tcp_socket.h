//tcp_sockets.h
#ifndef __TCP_SOCKET_H_INCLUDED__
#define __TCP_SOCKET_H_INCLUDED__

#include <cstdio>
#include <sys/socket.h>
#include <stdlib.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <cstring>
#include <string>
#include <iostream>
#include <vector>

struct myError {
	int error;
	myError(int e): error{e} {}
};

void printError(std::string usrMsg, myError err);

void printError(std::string usrMsg);

namespace tcps {

const int IP_ERROR = -1, IP_INVALID = 0,
		ENDPOINT_FAILURE = -1, CONNECT_FAILURE = -1,	BIND_FAILURE = -1,
		LISTEN_FAILURE = -1,	ACCEPT_FAILURE = -1,
		LISTEN_MAX_QUEUE_LEN = 3;

class tcp_socket {
	public:
		tcp_socket(): sock_{-1}, port_{-1}, address_{""} { 
			std::memset(&socket_addr_, '0', sizeof(socket_addr_)); }
		tcp_socket(int port, std::string address = ""):
			sock_{-1}, port_{port}, address_{address} {
				std::memset(&socket_addr_, '0', sizeof(socket_addr_)); }
		~tcp_socket() { close(sock_); }
		std::string getAddress() const;
		int getPort() const;
		//template<typename containerT> bool sendData(const containerT &data) const;
		template<typename T> bool sendData(T* data, int len) const;
		template<typename T> bool sendData(T data) const;
		//template<typename containerT>	containerT receiveData(int len);
		template<typename T> T* receiveData(int len);
		template<typename T> T receiveData();
	protected:
		int sock_;
		std::string address_;
		int port_;
		struct sockaddr_in socket_addr_;
};

class tcp_client : public tcp_socket {
	public:
		using tcp_socket::tcp_socket;
		tcp_client(int port, std::string address): tcp_socket(port, address) {
			if (!connectSocket()) printError("Error creating socket"); }
		bool connectSocket(int port, std::string address);
	private:
		bool connectSocket();
};

class tcp_server : public tcp_socket {
	public:
		using tcp_socket::tcp_socket;
		tcp_server(int port): tcp_socket(port) {
			if (!connectSocket()) printError("Error creating socket"); }
		bool bindSocket();
		bool bindSocket(int port);
		bool acceptSocket();
		bool connectSocket(int port);
	private:
		int oldSock_{-1};
		bool connectSocket();
};

template<class tcp_T>
class tcp_socket_array {
	public:
		tcp_socket_array(): numSockets_{-1}, initialPort_{-1}, address_{""} {}
		tcp_socket_array(int numSockets, int port, std::string address = ""):
			numSockets_{numSockets}, initialPort_{port}, address_{address} {}
		int getNumSockets() const { return numSockets_; }
		int getInitialPort() const { return initialPort_; }
		std::string getAddress() const { return address_; }
		tcp_T& operator[] (int x) { return sockfdArray_[x]; }
	protected:
		int numSockets_;
		int initialPort_;
		std::string address_;
		std::vector<tcp_T> sockfdArray_;
};

class tcp_client_array: public tcp_socket_array<tcp_client> {
	public:
		tcp_client_array(int numSockets, int portno, std::string address): 
			tcp_socket_array(numSockets, portno, address) {	connectSockets(); }
		bool connectSockets();
		bool connectSockets(int numSockets, int port, std::string address);
};

class tcp_server_array: public tcp_socket_array<tcp_server> {
	public:
		tcp_server_array(int numSockets, int port): 
			tcp_socket_array(numSockets, port) { acceptSockets(); }
		bool acceptSockets();
		bool acceptSockets(int numSockets, int port);
};

/*
template<typename containerT> bool tcp_socket::sendData(const containerT &data) const {
	bool wasSendSuccessful;
	//Send some data
	if (send(sock_, data.data(), data.size() * sizeof(data[0]), 0) == -1) {
		perror("Send failed : ");
		wasSendSuccessful = false;
	} else {
		wasSendSuccessful = true;
	}
	return wasSendSuccessful;
}
*/

template <typename T> bool tcp_socket::sendData(T* data, int len) const {
	bool wasSendSuccessful;
	//Send some data
	if (send(sock_, data, len * sizeof(T), 0) == -1) {
		printError("send failed", myError{errno});
		wasSendSuccessful = false;
	} else {
		wasSendSuccessful = true;
	}
	return wasSendSuccessful;
}

template<typename T> bool tcp_socket::sendData(T data) const {
	bool wasSuccess;
	int numBytes = send(sock_, &data, sizeof(data), 0);
	if (numBytes == -1) {
		printError("Send failed", myError{errno});
		wasSuccess = false;
	} else if (numBytes != sizeof(data)) {
		printError("Send failed. Not all data sent");
	} else {
		wasSuccess = true;
	}
	return wasSuccess;
}

/*
template<typename containerT> containerT tcp_socket::receiveData(int len) {
	containerT data(len);
	if (recv(sock_, data.data(), data.size() * sizeof(data[0]), 0) == -1)
		puts("recv failed");
	return data;
}
*/

template<typename T> T* tcp_socket::receiveData(int len) {
	if (len < 1) { printError("Expected length non-Existant"); return -1; }
	T *data = new T[len];
	int numBytes = recv(sock_, data, len * sizeof(data[0]), MSG_WAITALL);
	if (numBytes == -1)
		printError("Receive failed", myError{errno});
	else if (numBytes != len * sizeof(data[0]))
		printError("Receive failed. Not all data received");
	return data;
}

template<typename T> T tcp_socket::receiveData() {
	T data;
	int numBytes = recv(sock_, &data, sizeof(data), MSG_WAITALL);
	if (numBytes == -1)
		printError("Receive failed", myError{errno});
	else if (numBytes != sizeof(data))
		printError("Receive failed. Not all data received");
	return data;
}

}  //Namespace tcps
#endif
