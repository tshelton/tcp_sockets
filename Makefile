ST_COMPILER ?= g++-7 -std=c++17 -g 
INCLUDES :=
LIBRARIES :=

#Building Code Here
all: build
	build: server client #newServer

client.o: client.cpp tcp_socket.h
		$(EXEC) $(HOST_COMPILER) $(INCLUDES) -o $@ -c $<

server.o: server.cpp tcp_socket.h
		$(EXEC) $(HOST_COMPILER) $(INCLUDES) -o $@ -c $<

#newServer.o: newServer.cpp
	$(EXEC) $(HOST_COMPILER) $(INCLUDES) -o $@ -c $<

events.o: events.cpp events.h
		$(EXEC) $(HOST_COMPILER) $(INCLUDES) -o $@ -c $<

tcp_socket.o: tcp_socket.cpp tcp_socket.h
		$(EXEC) $(HOST_COMPILER) $(INCLUDES) -o $@ -c $<

client: client.o tcp_socket.o
		$(EXEC) $(HOST_COMPILER) $(INCLUDES) -o $@ $+ $(LIBRARIES)

server: server.o tcp_socket.o
		$(EXEC) $(HOST_COMPILER) $(INCLUDES) -o $@ $+ $(LIBRARIES)

#newServer: newServer.o
	$(EXEC) $(HOST_COMPILER) $(INCLUDES) -o $@ $+ $(LIBRARIES)

clean: 
		rm -f server client server.o client.o events.o tcp_socket.o


