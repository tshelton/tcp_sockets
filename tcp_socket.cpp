//tcp_socket.cpp
#ifndef __TCP_SOCKET_CPP_INCLUDED__
#define __TCP_SOCKET_CPP_INCLUDED__

#include "tcp_socket.h"

inline void printError(std::string usrMsg, myError err) {
	char buff[256];
	char const *errMsg = strerror_r(err.error, buff, 256);
	std::cerr << usrMsg << ": " << errMsg << std::endl;
}

inline void printError(std::string usrMsg) {
	std::cerr << usrMsg << std::endl;
}

namespace tcps {
//-----------------------------------------
//			TCP_SOCKET
//-----------------------------------------
//------------------------
//		Gets
//------------------------
std::string tcp_socket::getAddress() const { return address_; }

int tcp_socket::getPort() const { return port_; }

//-----------------------------------------
//			TCP_CLIENT
//-----------------------------------------
//------------------------
//		ConnectSocket(overloadx1)
//------------------------
// Creates and connects a socket
// Use if the constructor wasn't
bool tcp_client::connectSocket(int portno, std::string address) {
	// Check if socket was alread created
	bool wasSuccess;
	if (sock_ != -1) {
		// Abort if so
		printError("Socket already exits");
		wasSuccess = false;
	} else {
		// Otherwise, store info and create etc
		port_ = portno;
		address_ = address;
		wasSuccess = connectSocket();
	}
	return wasSuccess;
}

// Actual work done, never directly called outside of class
// TODO: Make this function more robust, e.g. handle more than just plain ip
bool tcp_client::connectSocket() {	
	bool wasConnexionSuccess;
	//Create socket
	if ((sock_ = socket(AF_INET, SOCK_STREAM, 0)) == ENDPOINT_FAILURE) {
		printError("Could not create socket", myError{errno});
		wasConnexionSuccess = false;
	} else {
		std::cout << "Socket created on port: " << port_ << std::endl;
	}
	//Setup address structure
	socket_addr_.sin_family = AF_INET;
	socket_addr_.sin_port = htons(port_);
	//Convert IPv4 and IPv6 addresses from text to binary form
	int addr_ip_res = inet_pton(AF_INET, address_.c_str(), &socket_addr_.sin_addr);
	if (addr_ip_res == IP_ERROR) {
		printError("af does not contain a valid address family", myError{errno});
		wasConnexionSuccess = false;
	} else if(addr_ip_res == IP_INVALID) {
		std::cerr << "Input isn't a valid IP address\n";
		 wasConnexionSuccess  = false;
	} else {  //Success 
		//Connect to remote server
		if (connect(sock_, reinterpret_cast<struct sockaddr*>(&socket_addr_), 
					sizeof(socket_addr_)) == CONNECT_FAILURE) {
			printError("Connect failed", myError{errno});
			wasConnexionSuccess = false;
		} else {
			std::cout << "Connected on port: " << port_ << std::endl;;
			wasConnexionSuccess = true;
		}
	}
	return wasConnexionSuccess;
}

//-----------------------------------------
//		TCP_SERVER
//-----------------------------------------
//------------------------
//	 ConnectSocket 
//------------------------
// Convenience functions for single socket use
// 	Creates, binds, sets to listen, and accepts for the socket
bool tcp_server::connectSocket(int port) {
	bool didConnect;
	// Check is socket already created
	if (sock_ != -1) {
		// If so, abort
		printError("Socket already exits");
		didConnect = false;
	} else {
		// Else, store values and proceed
		port_ = port;
		didConnect = connectSocket();
	}
	return didConnect;
}

// Internal function to check steps
inline bool tcp_server::connectSocket() {
	bool didConnect;
	// Create, bind, and set to listen
	if (!(didConnect = bindSocket())) {
		printError("Error creating socket");
	} else {  // Accept connexion
		if (!(didConnect = acceptSocket()))
			printError("Error accepting socket");
	}
	return didConnect;
}

//------------------------
// 	BindSocket(overloadx1)
//------------------------
// Creates and binds socket, also set to listen
// This is for outside interface step
bool tcp_server::bindSocket(int port) {
	bool didConnect;
	// Check is socket already created
	if (sock_ != -1) {
		// If so, abort
		printError("Socket already exits");
		didConnect = false;
	} else {
		// Else, store values and proceed
		port_ = port;
		didConnect = bindSocket();
	}
	return didConnect;
}

bool tcp_server::bindSocket() {
	bool didConnect;
	// Set up address structue
	socket_addr_.sin_family = AF_INET;
	socket_addr_.sin_addr.s_addr = INADDR_ANY;
	socket_addr_.sin_port = htons(port_);
	// At each step, check for failure, if failed: print error and stop
	// Create socket
	if ((oldSock_ = socket(AF_INET, SOCK_STREAM, 0)) == ENDPOINT_FAILURE) {
		printError("Socket failed", myError{errno});
		didConnect = false;
		
	} else {
		// Bind socket to port
		if (bind(oldSock_, (struct sockaddr*)&socket_addr_, 
					sizeof(socket_addr_)) == BIND_FAILURE) {
			printError("Bind failed", myError{errno});
			didConnect = false;
		} else {
			// Set socket to listen for client
			if (listen(oldSock_, LISTEN_MAX_QUEUE_LEN) == LISTEN_FAILURE) {
				printError("Listen failed", myError{errno});
				didConnect = false;
			} else {
				// Should've been successful if we get here
				didConnect = true;
			}
		}
	}
	return didConnect;
}

//------------------------
// 	AcceptSocket
//------------------------
// Accepts the socket connexion to client
bool tcp_server::acceptSocket() {
	bool wasSuccess;
	// Accept the connexion
	int addrLen = sizeof(socket_addr_);
	if ((sock_ = accept(oldSock_, (struct sockaddr *)&socket_addr_, 
					(socklen_t*)&addrLen)) == ACCEPT_FAILURE) {
		printError("Accept failed", myError{errno});
		wasSuccess = false;
	} else {
		std::cout << "Accepted on port " << port_ << std::endl;
		wasSuccess = true;
	}
	return wasSuccess;
}

//-----------------------------------------
// 		TCP_CLIENT_ARRAY
//-----------------------------------------
//------------------------
// 	connectSockets (overload x1)
//------------------------
// Overload to use if not using constructor
bool tcp_client_array::connectSockets(int numSockets, int port, std::string address) {
	bool wasSuccess;
	// Check if sockets already created
	if (numSockets_ != -1) {
		// If so, abort
		printError("Sockets already exist", myError{errno});
		wasSuccess = false;
	} else {
		// Otherwise, store values and create etc
		numSockets_ = numSockets;
		initialPort_ = port;
		address_ = address;
		wasSuccess = connectSockets();
	}
	return wasSuccess;
}

// Actually creates and connects sockets
bool tcp_client_array::connectSockets() {
	// Allocate enough of the vector
	// Not reserve because sockets cannot move or be copied!!
	sockfdArray_.resize(numSockets_);
	// For each socket
	bool wasSuccessful = false; int curPort = initialPort_;
	for (auto& sock: sockfdArray_) {
		// Create and connect
		wasSuccessful = sock.connectSocket(curPort++, address_);	
	}
	return wasSuccessful;
}

//-----------------------------------------
// 		TCP_SERVER_ARRAY
//-----------------------------------------
//------------------------
//		AcceptSockets (overloadx1)
//------------------------
// Creates, bind, sets to listen, and accepts for each socket
// Overload if constructor wasn't used
bool tcp_server_array::acceptSockets(int numSockets, int port) {
	bool wasSuccess;
	// Check if sockets were already created
	if (numSockets_ != -1) {
		// If so, abort
		printError("Sockets already exist");
		wasSuccess = false;
	} else {
		// Else, store values and create etc
		numSockets_ = numSockets;
		initialPort_ = port;
		wasSuccess = acceptSockets();
	}
	return wasSuccess;
}

// Actually does the work
bool tcp_server_array::acceptSockets() {
	// Allocate enough of the vector
	// Not reserve because sockets cannot move or be copied!!
	sockfdArray_.resize(numSockets_);
	bool wasSuccessful = false;
	int curPort = initialPort_;
	// For each sockets
	for (auto& sock: sockfdArray_) 
		// Create, bind, and set to listen
		wasSuccessful = sock.bindSocket(curPort++);
	for (auto& sock: sockfdArray_)
		// Accept connexion
		wasSuccessful = sock.acceptSocket();
	// Note, the accept is seperated from the rest because otherwise it doesn't
	// work past the first, unsure if expected behavior or not
	return wasSuccessful;
}

}  //Namespace tcps
#endif
